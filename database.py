import json
import random
import string

from peewee import MySQLDatabase, Model, CharField, BlobField, AutoField, IntegrityError, DoesNotExist, BigIntegerField
from peewee import IntegerField, ForeignKeyField, DateTimeField
import os

db = MySQLDatabase(None)


def get_db_uri():
    user = os.getenv("DB_USER")
    password = os.getenv("DB_PASSWORD")
    host = os.getenv("DB_HOST")
    port = os.getenv("DB_PORT")
    database = os.getenv("DB_DATABASE")

    return "mysql://{}:{}@{}:{}/{}".format(
        user,
        password,
        host,
        port,
        database
    )


def connect(create_tables=False) -> None:
    """Initializes the database session and connects to the database
    :param create_tables: Creates database tables [default: False]
    """
    user = os.getenv("DB_USER")
    pw = os.getenv("DB_PASSWORD")
    host = os.getenv("DB_HOST")
    port = os.getenv("DB_PORT")
    name = os.getenv("DB_DATABASE")

    db.init(name, host=host, port=int(port), user=user, password=pw)

    db.connect()

    if create_tables:
        db.create_tables([AppState, AppUser, AppWallet,AppWalletRequest, AppUserWallet, AppPendingTX, AppPendingTxSignature])
        db.execute_sql("REPLACE INTO `appstate` (`id`, `message`, `menu`, `prev_state`, `next_state`) VALUES \
	(1, 'Welcome to UT-BCDL-G1 Bitcoin Multi-sig coordinator!', '[[[\"➕ Create a wallet\", \"create_a_wallet\"]],[[\"✔️ Choose a wallet\", \"choose_wallet\"]]]', 1, 1), \
	(2, 'Please enter the total number of co-signers (max: 15):', '', 1, 3), \
	(3, 'Please enter the minimum co-signers required to sign a transaction:', '', 2, 4), \
	(4, 'Now please enter a name for this wallet:', '', 3, 1), \
	(5, 'Please choose a wallet:', '', 1, 6), \
	(6, 'Please select one of followings:', '[[[\"Create a transaction\", \"send_a_transaction\"],[\"Receive bitcoin\",\"receive_a_transaction\"]],[[\"Sign a transaction\",\"sign_transaction\"],[\"List transactions\",\"list_transactions\"]]]', 5, 6), \
	(7, 'Please enter the destination bitcoin address:', '', 6, 8), \
	(8, 'Please enter the amount in satoshis:', '', 7, 9), \
	(9, 'Please select the fee for transaction:', '[[[\"Low\",\"low\"],[\"Normal\", \"normal\"],[\"High\", \"high\"]]]', 8, 1), \
	(10, 'Please sign the above transactions.', NULL, 6, 6), \
	(41, '-', NULL, 6, 6);")
                


class BaseModel(Model):
    id = AutoField()

    class Meta:
        database = db


class AppState(BaseModel):
    message = CharField()
    menu = CharField(null=True)
    prev_state = IntegerField()
    next_state = IntegerField()


class AppUser(BaseModel):
    telegram_id = BigIntegerField(unique=True)
    state_id = ForeignKeyField(AppState, default=1)
    nickname = CharField()
    variables = CharField(default="{}")
    last_msg_id = BigIntegerField()

    def set_last_msg_id(self, new_last_msg_id):
        AppUser.update({AppUser.last_msg_id: new_last_msg_id}).where(AppUser.id == self.id).execute()

    def next_state(self):
        AppUser.update({AppUser.state_id: self.state_id.next_state}).where(AppUser.id == self.id).execute()
        return self.state_id.next_state

    def set_state(self, new_state):
        AppUser.update({AppUser.state_id: new_state}).where(AppUser.id == self.id).execute()

    @property
    def all_variables(self):
        newuser = AppUser.get(AppUser.id == self.id)
        return json.loads(str(newuser.variables))

    def set_variables(self, new_variables: dict):
        AppUser.update({AppUser.variables: json.dumps(new_variables)}).where(AppUser.id == self.id).execute()

    def set_variable(self, key, value):
        new_variables = self.all_variables
        new_variables[key] = value
        self.set_variables(new_variables)


class AppWallet(BaseModel):
    max_co_signers = IntegerField()
    min_co_signers = IntegerField()
    initiator_user_id = ForeignKeyField(AppUser, backref='users')
    name = CharField()
    latest_update = DateTimeField()

    def set_latest_update(self,latest_update):
        AppWallet.update({AppWallet.latest_update: latest_update}).where(AppWallet.id == self.id).execute()

class AppWalletRequest(BaseModel):
    token = CharField()
    wallet_id = ForeignKeyField(AppWallet, backref='walletrequests')

    @classmethod
    def generate_requests(cls, amount, wallet_id):
        wallet_requests = [{'token': ''.join(random.choice(string.ascii_letters) for i in range(64)), 'wallet_id': wallet_id} for x in range(amount)]
        AppWalletRequest.insert(wallet_requests).execute()
        return wallet_requests


class AppUserWallet(BaseModel):
    user_id = ForeignKeyField(AppUser, backref='users')
    wallet_id = ForeignKeyField(AppWallet, backref='wallets')


class AppPendingTX(BaseModel):
    from_wallet_id = ForeignKeyField(AppWallet, backref='pendingtx')
    to_address = CharField()
    amount = IntegerField()
    fee_str = CharField()
    initiator_user_id = ForeignKeyField(AppUser, backref='users')
    expiration_timestamp = DateTimeField()


class AppPendingTxSignature(BaseModel):
    pending_tx_id = ForeignKeyField(AppPendingTX, backref='pendingtxs')
    user_id = ForeignKeyField(AppUser, backref='users')
    signed = IntegerField()

    def sign(self):
        self.signed = 1
        self.save()
