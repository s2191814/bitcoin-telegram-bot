import os
import sys
from dotenv import load_dotenv
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler
import command
from database import connect
from util import cron

load_dotenv()

class TelegramWrapper:
    def __init__(self, token):
        self._token = token
        self._updater = Updater(token=token)
        self._dispatcher = self._updater.dispatcher
        self.registerCommands()
        self.connect_database()

    def registerCommands(self):
        for key in command.COMMANDS:
            self._dispatcher.add_handler(CommandHandler(key, command.COMMANDS[key]))
        self._dispatcher.add_handler(CallbackQueryHandler(command.callback_handler))
        self._dispatcher.add_handler(MessageHandler(Filters.all, command.message_handler))

    def start_polling(self):
        self._updater.start_polling()
        self._updater.idle()

    def connect_database(self):
        connect(False)


if __name__ == "__main__":
    telegramWrapper = TelegramWrapper(os.getenv("TOKEN"))
    n = len(sys.argv)
    if n>1 and sys.argv[1]=='cron':
        print("cron")
        cron(telegramWrapper._updater)
    else:
        telegramWrapper.start_polling()
