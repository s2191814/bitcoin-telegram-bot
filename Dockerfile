FROM python:3.10.0
WORKDIR /usr/src/app

COPY requirements.txt .
RUN apt-get -y update
RUN pip3 install -r requirements.txt
RUN apt-get autoremove -y && apt-get -y clean

COPY . .
CMD ["python3", "-u", "main.py"]