from peewee import DoesNotExist
from commands.util import *
from database import AppUser, AppWalletRequest, AppUserWallet, AppState
from util import generate_wallets,get_default_menu
from commands.wallet import choose_wallet,wallet_options
from telegram import ParseMode


def start(update: Update, context: CallbackContext):
    msg = update.effective_message.text
    params = msg.split(" ")
    telegram_user = update.effective_user
    user, created = AppUser.get_or_create(telegram_id=telegram_user.id, nickname=telegram_user.full_name)
    if len(params) == 2:
        token = params[1]
        request = None
        try:
            request = AppWalletRequest.get(AppWalletRequest.token == token)
        except DoesNotExist:
            send_msg(update, context, user, "❌ Token does not exists, something went wrong.", True)

        user_wallet, created = AppUserWallet.get_or_create(user_id=user.id, wallet_id=request.wallet_id)
        # TODO remove or True
        if created:
            request.delete_instance()
            send_msg(update, context, user, "✅ Successfully joined the wallet!", True)
            count = AppWalletRequest.select().where(AppWalletRequest.wallet_id == request.wallet_id).count()
            if count == 0:
                user_wallets = AppUserWallet.select().where(AppUserWallet.wallet_id == request.wallet_id)
                wallets, mnemonics = generate_wallets(user_wallets)
                for i in range(user_wallets.count()):
                    user = user_wallets[i].user_id
                    wallet = user_wallets[i].wallet_id
                    context.bot.send_message(chat_id=user.telegram_id, text="Wallet name: {}\n"
                                                                            "Your wallet's 12-word mnemonic phrase:\n"
                                                                            "<code>{}</code>".format(wallet.name, mnemonics[i]),\
                                                                            parse_mode=ParseMode.HTML)
                pass
        else:
            send_msg(update, context, user, "❌ You have already joined this wallet.", no_menu=True,custom_menu=get_default_menu(only_home=True))
            return

    execute_state(update, context, user, 1)


def back(update: Update, context: CallbackContext):
    telegram_user = update.effective_user
    user, created = AppUser.get_or_create(telegram_id=telegram_user.id, nickname=telegram_user.full_name)
    state = AppState.get(AppState.id == user.state_id)

    # All the extra functions that need to be called when back is called
    EXTRA_OPTIONS = {
        5: [choose_wallet],
        6: [wallet_options]
    }

    try:
        functions = EXTRA_OPTIONS[state.prev_state]
        user.set_state(state.prev_state)
        user=AppUser.get(id=user.id)
        for f in functions:
            try:
                f(update, context, user)
            except:
                f(update, context)
    except KeyError:
        execute_state(update, context, user, state.prev_state)
