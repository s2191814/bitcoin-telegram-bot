import datetime

from bitcoinlib.encoding import EncodingError
from bitcoinlib.keys import Address
from commands.util import *
from commands.wallet import choose_wallet
from database import AppUser, AppPendingTX, AppPendingTxSignature, AppUserWallet, AppWallet
from util import *


def receive_a_transaction(update: Update, context: CallbackContext):
    telegram_user = update.effective_user
    user, created = AppUser.get_or_create(telegram_id=telegram_user.id, nickname=telegram_user.full_name)
    variables = user.all_variables
    if not "wallet_id" in variables:
        choose_wallet(update, context, user)
        return
    address = get_new_address(variables['wallet_id'], user.id)
    user.set_state(41)
    send_msg(update,context,user,"<code>" + address + "</code>")
    
def list_transactions(update: Update, context: CallbackContext):
    telegram_user = update.effective_user
    user, created = AppUser.get_or_create(telegram_id=telegram_user.id, nickname=telegram_user.full_name)
    variables = user.all_variables
    if not "wallet_id" in variables:
        choose_wallet(update, context, user)
        return
    wallet_id=variables["wallet_id"]
    txs=get_wallet_transactions(wallet_id, user.id)
    if not txs:
        txs="❗️This wallet has had no transactions yet."

    user.set_state(41)
    send_msg(update, context, user, txs)


def send_a_transaction(update: Update, context: CallbackContext):
    telegram_user = update.effective_user
    user, created = AppUser.get_or_create(telegram_id=telegram_user.id, nickname=telegram_user.full_name)
    if user.state_id.id != 6:
        send_msg(update, context, user, "❌ You cannot do this operation right now.")
    else:
        execute_state(update, context, user, 7)
    pass


def transaction_address(update: Update, context: CallbackContext, user: AppUser):
    msg = update.effective_message.text
    try:
        address = Address(msg, network=os.getenv("BTC_NETWORK"))
        user.set_variable("address", msg)
        execute_state(update, context, user)
    except EncodingError:
        send_msg(update, context, user, "❌ This is not a valid address. Please enter a valid address:")


def transaction_amount(update: Update, context: CallbackContext, user: AppUser):
    msg = update.effective_message.text
    try:
        amount = float(msg)
        user.set_variable("amount", amount)
        execute_state(update, context, user)
    except ValueError:
        send_msg(update, context, user, "❌ This is not a number, try again:")


def transaction_fee(update: Update, context: CallbackContext, user: AppUser):
    query = update.callback_query.data
    if query == "low" or query == "normal" or query == "high":
        user.set_variable("fee", query)
    else:
        send_msg(update, context, user, "❗️Please choose one item from the menu")

    variables = user.all_variables
    #has_funds, error = has_enough_funds(variables["wallet_id"], user.id, variables["address"], variables["amount"], variables["fee"])
    #if not has_funds:
    #    send_msg(update, context, user, "❌ There are not enough funds in the wallet to do this transaction.")
    #elif error:
    #    send_msg(update, context, user, "❌ Something went wrong when creating this transaction.")
    #else:
    user_wallets = AppUserWallet.select().where(AppUserWallet.wallet_id == variables["wallet_id"])
    expire = datetime.datetime.now() + datetime.timedelta(days=1)
    pending_tx = AppPendingTX.create(from_wallet_id=variables["wallet_id"],
                                        to_address=variables["address"],
                                        amount=variables["amount"],
                                        fee_str=variables["fee"],
                                        initiator_user_id=user.id,
                                        expiration_timestamp=expire)

    pending_txs = [{
        'pending_tx_id': pending_tx.id,
        'user_id': uw.user_id.id,
        'signed': 1 if uw.user_id.id == user.id else 0
    } for uw in user_wallets]
    appwallet=AppWallet.get(AppWallet.id==variables["wallet_id"])

    AppPendingTxSignature.insert(pending_txs).execute()
    for uw in user_wallets:
        if uw.user_id.id != user.id:
            msg = "👉 {} has created a new transaction in your multi-signature wallet: <b>{}</b>\nPlease go to the wallet menu to sign the transaction."\
            .format(user.nickname,appwallet.name)
            message=context.bot.send_message(chat_id=uw.user_id.telegram_id, text=msg,parse_mode=ParseMode.HTML, reply_markup=InlineKeyboardMarkup(get_default_menu(True)))
            uw.user_id.set_last_msg_id(message.message_id)

    msg = "✅ Successfully created the transaction. Please wait for the required number of co-signers to sign the transaction."
    send_msg(update, context, user, msg, no_menu=True,custom_menu=get_default_menu(only_home=True))


def show_sign_transaction(update: Update, context: CallbackContext):
    telegram_user = update.effective_user
    user, created = AppUser.get_or_create(telegram_id=telegram_user.id, nickname=telegram_user.full_name)
    variables = user.all_variables
    transactions = (AppPendingTX.select()
                    .join(AppPendingTxSignature)
                    .where(AppPendingTxSignature.user_id == user)
                    .where(AppPendingTX.expiration_timestamp >= datetime.datetime.now())
                    .where(AppPendingTX.from_wallet_id == variables["wallet_id"])
                    .where(AppPendingTxSignature.signed == 0))

    if transactions.count() == 0:
        user.set_state(41)
        send_msg(update, context, user, "❗️There are no pending transactions for you to sign")
        return

    for tx in transactions:
        custom_menu = [[InlineKeyboardButton("Sign", callback_data=tx.id)]]
        msg = "To: <code>{}</code> \n" \
               "Amount: {} satoshis \n" \
              "Initiator: {}".format(tx.to_address, tx.amount, tx.initiator_user_id.nickname)
        send_msg(update, context, user, msg, custom_menu=custom_menu)
        user.set_state(10)
        
    #execute_state(update, context, user, new_state_id=10)
    pass


def sign_transaction(update: Update, context: CallbackContext, user: AppUser):
    transaction_id = update.callback_query.data
    tx_signatures = (AppPendingTxSignature.select()
                     .join(AppPendingTX)
                     .where(AppPendingTX.id == transaction_id))

    tx_signature = tx_signatures.where(AppPendingTxSignature.user_id == user)[0]
    tx = tx_signature.pending_tx_id
    tx_signature.sign()

    signers = tx_signatures.where(AppPendingTxSignature.signed == 1)
    signed = signers.count()
    send_msg(update, context, user, "✅ Signed the transaction.", no_menu=True)
    if signed >= tx.from_wallet_id.min_co_signers:
        send, error = do_transaction(signers, tx)
        if error or not send:
            send_msg(update, context, user, "❌ Something went wrong.", no_menu=True)
        else:
            send_msg(update, context, user, "✅ Transaction has been sent to the bitcoin network", get_default_menu(True))

        AppPendingTxSignature.delete().where(AppPendingTxSignature.pending_tx_id == tx).execute()
        tx.delete_instance()
    else:
        send_msg(update, context, user, "Waiting for more people to sign the transaction", no_menu=True)

    execute_state(update, context, user)
