import ast

from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram import Update,ParseMode
from telegram.ext import CallbackContext
from database import AppState, AppUser


def execute_state(update: Update, context: CallbackContext, user: AppUser, new_state_id=None, custom_menu=None, custom_message=None):
    if new_state_id:
        if new_state_id == 1:
            user.set_variables({})
        user.set_state(new_state_id)
        reply_msg, reply_markup = generate_message(new_state_id, custom_menu, custom_message)
    else:
        reply_msg, reply_markup = generate_message(user.next_state(), custom_menu, custom_message)

    msg_id = user.last_msg_id

    if update.callback_query and user.state_id.id != 1:  # edit messsage
        if msg_id:
            try:
                context.bot.edit_message_text(text=reply_msg, chat_id=user.telegram_id, parse_mode=ParseMode.HTML, message_id=msg_id, reply_markup=reply_markup)
                return
            except:
                pass
    else:  # do not edit the last message, delete the last one and send a new one
        if msg_id:
            try:
                delete_last_msg(user, context)
            except:
                pass

    msg = update.effective_message.reply_text(reply_msg, reply_markup=reply_markup, parse_mode=ParseMode.HTML)
    user.set_last_msg_id(msg.message_id)


def delete_last_msg(user: AppUser, context: CallbackContext):
    msg_id = user.last_msg_id
    if msg_id:
        try:
            context.bot.delete_message(chat_id=user.telegram_id, message_id=msg_id)
        except:
            pass


def send_msg(update: Update, context: CallbackContext, user: AppUser, message, no_menu=False, custom_menu=None):
    delete_last_msg(user, context)

    if not custom_menu:
        custom_menu = []

    if no_menu:
        reply_markup = InlineKeyboardMarkup(custom_menu)
    else:
        reply_markup = InlineKeyboardMarkup(custom_menu + get_default_menu())

    msg = update.effective_message.reply_text(message, reply_markup=reply_markup,parse_mode=ParseMode.HTML)
    #if not no_menu:
    user.set_last_msg_id(msg.message_id)


def get_menu(menu_string):
    menu_string = ast.literal_eval(menu_string)
    menu = [[InlineKeyboardButton(y[0], callback_data=y[1]) for y in x] for x in menu_string]
    return menu


def get_default_menu(only_home=False):
    if only_home==True:
         menu_string = "[[[\"🏠\",\"start\"]]]"
    else:
        menu_string = "[[[\"🔙\",\"back\"],[\"🏠\",\"start\"]]]"
    return get_menu(menu_string)


def generate_message(state_id, extra_menu=None, initial_msg=None):
    if extra_menu is None:
        extra_menu = []
    state = AppState.get(AppState.id == state_id)
    if initial_msg is not None:
        reply_msg = initial_msg + "\n" + state.message
    else:
        reply_msg = state.message

    if not state.menu:
        menu = get_default_menu()
    else:
        if state_id != 1:
            menu = get_menu(state.menu) + get_default_menu()
        else:
            menu = get_menu(state.menu)

    menu = extra_menu + menu
    reply_markup = InlineKeyboardMarkup(menu)
    return reply_msg, reply_markup
