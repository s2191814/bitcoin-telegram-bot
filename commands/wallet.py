import os

from commands.util import *
from util import get_wallet_info
from database import AppUser, AppWallet, AppUserWallet, AppWalletRequest


def create_wallet(update: Update, context: CallbackContext):
    telegram_user = update.effective_user
    user, created = AppUser.get_or_create(telegram_id=telegram_user.id, nickname=telegram_user.full_name)
    execute_state(update, context, user, 2)


def enter_co_signers(update: Update, context: CallbackContext, user: AppUser):
    msg = update.effective_message.text
    try:
        co_signers = int(msg)
        if co_signers > 15 or co_signers < 2:
            send_msg(update, context, user, "❌ Please enter a number between 2 and 15:")
        else:
            user.set_variable('max_co_signers', co_signers)
            execute_state(update, context, user)

    except ValueError:
        send_msg(update, context, user, "❌ Invalid input. Please try again:")


def minimum_co_signers(update: Update, context: CallbackContext, user: AppUser):
    msg = update.effective_message.text
    try:
        min_signers = int(msg)
        max_signers = int(user.all_variables['max_co_signers'])
        if min_signers > max_signers or min_signers < 1:
            message = "❌ Please enter a value between 1 and " + str(user.all_variables['max_co_signers']) + ":"
            send_msg(update, context, user, message)
            return
        user.set_variable('min_co_signers', min_signers)
        execute_state(update, context, user)

    except ValueError:
        send_msg(update, context, user, "❌ Invalid input. Please try again:")


def name_wallet(update: Update, context: CallbackContext, user: AppUser):
    msg = update.effective_message.text
    if len(msg) > 100:
        send_msg(update, context, user, "❌ The entered name is too long. Please try again:")
    else:
        user.set_variable('wallet_name', msg)
        variables = user.all_variables
        app_wallet = AppWallet.create(max_co_signers=variables['max_co_signers'],
                                      min_co_signers=variables['min_co_signers'],
                                      initiator_user_id=user.id,
                                      name=variables['wallet_name'])

        AppUserWallet.create(user_id=user.id, wallet_id=app_wallet.id)

        wallet_requests = AppWalletRequest.generate_requests(variables['max_co_signers'] - 1, app_wallet.id)
        send_msg(update, context, user, "✅ Multi-signature wallet has been registered!\n👉 Please forward each of these messages to one co-signer:", True)

        for request in wallet_requests:
            send_msg(update, context, user, "Please join this multi-signature wallet as co-signer:\n\n"
                                            "Wallet: {}\n"
                                            "{} of {} multi-signaure\n"
                                            "<a href='https://t.me/{}?start={}'>Join {} wallet</a>" \
                                            .format(variables['wallet_name'], \
                                            variables['min_co_signers'], \
                                            variables['max_co_signers'], \
                                            os.getenv("BOT_NAME"), \
                                            request["token"], \
                                            variables['wallet_name']), \
                                            True)

        execute_state(update, context, user)


def wallet_options(update: Update, context: CallbackContext, user: AppUser):
    # if the user types sth instead of clicking on a menu item

    if not update.callback_query:
        return

    if "wallet_id" in user.all_variables and update.callback_query.data=='back':
        wallet_id=user.all_variables["wallet_id"]
    else:
        wallet_id = update.callback_query.data
    # Does this wallet actually belong to this user?
    user_wallet = AppUserWallet.select().where(AppUserWallet.user_id == user).where(AppUserWallet.wallet_id == wallet_id)
    if not user_wallet:
        send_msg(update, context, user, "❌ You have no such a wallet.")
        choose_wallet(update, context)
        return
    # is this wallet completely created in bitcoinlib database? i.e. total co-signers joined?
    
    wallet_request = AppWalletRequest.select().where(AppWalletRequest.wallet_id == wallet_id).count()
    if wallet_request != 0:
        user.set_state(41)
        send_msg(update, context, user, "❌ This wallet has not been created yet. Please wait for the other co-signers to join.")
        return
    user.set_variable("wallet_id", wallet_id)
    wallet_info = get_wallet_info(wallet_id, user.id)
    execute_state(update, context, user, custom_message=wallet_info)


def choose_wallet(update: Update, context: CallbackContext, user=None):
    telegram_user = update.effective_user
    user = AppUser.get(telegram_id=telegram_user.id)
    user_wallets = AppUserWallet.select().where(AppUserWallet.user_id == user)
    if not user_wallets:
        send_msg(update, context, user, "❌ Currently, you have no wallets.", True)
        execute_state(update, context, user, 1)
        return
    
    custom_menu = [
                    [
                        InlineKeyboardButton( \
                                    "{} ({}-of-{})".format( \
                                uw.wallet_id.name, \
                                uw.wallet_id.min_co_signers, \
                                uw.wallet_id.max_co_signers), \
                                    callback_data=uw.wallet_id.id\
                                    ) \
                    ] \
                    for uw in user_wallets\
                ]

    execute_state(update, context, user, 5, custom_menu)
    pass
