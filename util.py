import os

from bitcoinlib.keys import HDKey
from bitcoinlib.mnemonic import Mnemonic
from bitcoinlib.wallets import Wallet, WalletError
from database import *
from telegram.ext import Updater
from commands.util import get_default_menu
from telegram import InlineKeyboardButton, InlineKeyboardMarkup,ParseMode
import database
from pprint import pprint

def get_wallet_name(wallet_id, user_id):
    return "wallet_{}user_{}".format(wallet_id, user_id)


def generate_klist(hd_keys, private):
    return [hd_keys[x] if x == private else hd_keys[x].public_master_multisig() for x in range(len(hd_keys))]


def generate_hd_keys(amount):
    mnemonics = [Mnemonic().generate(strength=128, add_checksum=True) for i in range(amount)]
    keys = [HDKey.from_passphrase(passphrase, witness_type='p2sh-segwit', network=os.getenv("BTC_NETWORK"), multisig=True) for passphrase in mnemonics]
    return keys, mnemonics
    pass


def generate_wallets(user_wallets):
    count = user_wallets.count()
    hd_keys, mnemonics = generate_hd_keys(count)
    wallets = []
    for i in range(count):
        uw = user_wallets[i]
        wallet_name = get_wallet_name(uw.wallet_id.id, uw.user_id.id)
        wallet = Wallet.create(wallet_name,
                               witness_type='p2sh-segwit',
                               sigs_required=uw.wallet_id.min_co_signers,
                               keys=generate_klist(hd_keys, i),
                               network=os.getenv("BTC_NETWORK"),
                               db_uri=database.get_db_uri())
        wallets.append(wallet)
    return wallets, mnemonics


def do_transaction(pending_tx_sigs, pending_tx):
    #try:
    wallet_id = pending_tx.from_wallet_id.id
    wallets = [get_wallet(wallet_id, sig.user_id.id) for sig in pending_tx_sigs]
    first_wallet = wallets[0]
    first_wallet.new_key()
    t = first_wallet.send_to(to_address=pending_tx.to_address,fee=1000, network=os.getenv("BTC_NETWORK"), amount=pending_tx.amount)
    t.sign()
    for w in wallets:
        if w is not wallets[0]:
            w.new_key()
            t = w.transaction_import(t)
            t.sign()
    if t.verify():
        t.send()
        return True, None
    else:
        return False, None
    #except:
    #    return False, "Something went wrong"

def has_enough_funds(wallet_id, user_id, address, amount, fee):
    wallet = get_wallet(wallet_id, user_id)
    has_enough_funds = True
    error = False
    #try:
    wallet.send_to(to_address=address, fee=fee, network=os.getenv("BTC_NETWORK"), amount=amount)
    return has_enough_funds, error
    #except WalletError as e:

        #error = True
        #if e.msg == "Not enough unspent transaction outputs found":
        #    has_enough_funds = False

    return has_enough_funds, error


def get_wallet(wallet_id, user_id):
    wallet = Wallet(get_wallet_name(wallet_id, user_id), db_uri=database.get_db_uri())
    return wallet


def get_new_address(wallet_id, user_id):
    all_user_wallets=AppUserWallet.select().where(AppUserWallet.wallet_id==wallet_id)
    for each in all_user_wallets:
        wallet = get_wallet(wallet_id, each.user_id.id)
        wallet.new_key()
    
    return wallet.addresslist(used=False)[0]


def get_wallet_info(wallet_id, user_id):
    wallet = get_wallet(wallet_id, user_id)
    appwallet = AppWallet.get(id=wallet_id)

    msg = "Wallet: <b>{}</b> \n" \
          "💰 Balance: {} \n" \
          "✍️ {} of {} multi-signaure\n" \
            .format(appwallet.name,
                                        int(wallet.balance()) ,
                                        appwallet.min_co_signers,
                                        appwallet.max_co_signers)
    return msg


def get_wallet_total_signers(wallet_id):
    appwallet = AppWallet.get(id=wallet_id)
    return appwallet.max_co_signers

def get_wallet_transactions(wallet_id, user_id):
    wallet = get_wallet(wallet_id, user_id)
    export=wallet.transactions_export(network=os.getenv("BTC_NETWORK"))
    if not export:
        return None
    export=export[::-1]
    #print(export)
    wallet_name=AppWallet.get(id=wallet_id).name
    result="📄 Transactions of wallet <b>{}</b>:\n\n".format(wallet_name)
    for item in export:
        result=result + get_transaction_info(wallet,item)
    
    return result


def get_transaction_info(wallet: Wallet,tx):
    transaction_hash=tx[1]
    tx_info=wallet.transaction(transaction_hash)
    status=tx_info.status
    fee=tx_info.fee
    from_address=tx[3]
    
    #if not fee:
    #    pprint(vars(tx_info))

    result= "⏰ " + tx[0].strftime("%Y-%m-%d %H:%M:%S")
    result=result + "     " +  ("📥" if (str(tx[2])=="in") else "📤") + "\n"
    result=result + "From: <code>" + str(from_address) + "</code>\n"
    result=result + "To: <code>" + str(tx[4]) + "</code>\n"
    result=result + "Amount: <code>" + str(tx[5]) + "</code>     Fee: <code>" + str(fee) + "</code>"
    result=result + "     Status: " + status + "\n"
    result=result + "<a href='https://www.blockchain.com/btc-testnet/tx/" + transaction_hash +"'>View on blockchain explorer</a>\n\n"
    return result

def cron(updater: Updater):
    wallets = AppUserWallet.select()
    for w in wallets:
        this_wallet_id=w.wallet_id
        appwallet=AppWallet.get(AppWallet.id == this_wallet_id)
        try:
            wallet = get_wallet(this_wallet_id, w.user_id)
        except:
            print("cant get the wallet")
            continue

        print("Updating {}".format(wallet))
        #wallet.scan()
        wallet.transactions_update(network=os.getenv("BTC_NETWORK"))
        export=wallet.transactions_export(include_new=True,network=os.getenv("BTC_NETWORK"))
        export=export[::-1]
        for each_tx in export:
            tx_datetime=each_tx[0]

            if not appwallet.latest_update or tx_datetime > appwallet.latest_update:
                appwallet.set_latest_update(tx_datetime)
                target_users=AppUserWallet.select().where(AppUserWallet.wallet_id == this_wallet_id)
                for each_user in target_users:
                    user=AppUser.get(AppUser.id == each_user.user_id)
                    target_user_telegram_id=user.telegram_id
                    msg="👉 New transaction for your wallet <b>" + appwallet.name + "</b>:\n\n"
                    msg+=get_transaction_info(wallet,each_tx)
                    send_msg=updater.bot.send_message(chat_id=target_user_telegram_id, text=msg,parse_mode=ParseMode.HTML, \
                                            reply_markup=InlineKeyboardMarkup(get_default_menu(True)))
                    user.set_last_msg_id(None)
            

    
