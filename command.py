from telegram import Update
from telegram.ext import CallbackContext

from commands.general import start, back
from commands.transaction import *
from commands.wallet import *
from database import AppUser


def callback_handler(update: Update, context: CallbackContext):
    query = update.callback_query.data
    try:
        f = COMMANDS[query]
        f(update, context)
    except KeyError:
        # Current state does not have a message handler, so do nothing
        # Try the message handler, to go to state function
        message_handler(update, context)
        pass


def message_handler(update: Update, context: CallbackContext):
    telegram_user = update.effective_user
    user, created = AppUser.get_or_create(telegram_id=telegram_user.id, nickname=telegram_user.full_name)
    state_user = user.state_id.id
    HANDLERS = {
        2: enter_co_signers,
        3: minimum_co_signers,
        4: name_wallet,
        5: wallet_options,
        7: transaction_address,
        8: transaction_amount,
        9: transaction_fee,
        20: receive_a_transaction,
        10: sign_transaction
    }
    try:
        f = HANDLERS[state_user]
        f(update, context, user)
    except KeyError:
        # Current state does not have a message handler, so do nothing
        pass


COMMANDS = {
    "start": start,
    "create_a_wallet": create_wallet,
    "send_a_transaction": send_a_transaction,
    "receive_a_transaction": receive_a_transaction,
    "list_transactions": list_transactions,
    "back": back,
    "choose_wallet": choose_wallet,
    "sign_transaction": show_sign_transaction,
}
